﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import ProductTableRow from './ProductTableRow'; 

class DeleteProductModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = { data: [], showModal: false };
		this.handleOpen = this.handleOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.confirmDelete = this.confirmDelete.bind(this);
	}

	confirmDelete() {
		axios({
			method: 'delete',
			url: '/Products/DeleteProduct/' + this.props.productId
		})
			.then(response => {
				//console.log(response.data);
				this.setState({ data: response.data,showModal: false  });
			})
			.catch(error => {
				console.log(error)
			})
	}
	handleOpen() {
		this.setState({ showModal: true });
	}
		
	closeModal() {
		this.setState({ showModal: false });
	}
	
	render() {
		return (
			<div>
				<Modal size="tiny" trigger={<Button onClick={this.handleOpen} color="red"><Icon name='trash' />Delete</Button>} open={this.state.showModal} >
					<Header content="Delete Product Record" />
					<Modal.Content>						
						<div>
							<h4>Are You sure ? You want to Delete This Record</h4>
						</div>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.confirmDelete} color="green">
							<Icon name="checkmark" />Confirm
						</Button>
					</Modal.Actions>
				</Modal>
			</div>
			);
	}
}
export default DeleteProductModal;