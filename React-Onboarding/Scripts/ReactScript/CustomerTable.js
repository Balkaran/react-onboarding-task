﻿import React, { Component } from 'react';
import { Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import CustomerTableRow from './CustomerTableRow';
import CustomerModal from './CustomerModal';


class CustomerTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = { data: [] };
	}

	componentDidMount(){
		//axios.get("/Customers/GetCustomerData")
		axios({
			method: 'get',
			url: '/Customers/GetCustomerData'
		})
		.then(response => {
			const data = response.data;
			//console.log(data);
			this.setState({ data });
		})
		.catch(error => {
			console.log(error)
		})
	}

	componentDidUpdate() {
		axios({
			method: 'get',
			url: '/Customers/GetCustomerData'
		})
			.then(response => {
				const data = response.data;
				//console.log(data);
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}
	render() {
		
	var rows = [];
		this.state.data.forEach(function (customer) {
			rows.push(<CustomerTableRow key={customer.Id} customerId={customer.Id}
				name={customer.CustomerName} address={customer.CustomerAddress} />);
			});
		return (
			<Table celled>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Customer Name</Table.HeaderCell>
						<Table.HeaderCell>Customer Address</Table.HeaderCell>
						<Table.HeaderCell>Action(Edit)</Table.HeaderCell>
						<Table.HeaderCell>Action(Delete)</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				<Table.Body>
					{rows}
				</Table.Body>

				<Table.Footer>
				</Table.Footer>
				</Table>
			);
	}

}
export default CustomerTable;



