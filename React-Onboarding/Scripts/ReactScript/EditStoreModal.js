﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';

class EditStoreModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			storeName: "", storeAddress: "",
			showModal: false, storeNameError: '', storeAddressError: ''
		};
		this.handleOpen = this.handleOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.btnSave = this.btnSave.bind(this);
	}

	handleOpen() {
		this.setState({
			showModal: true, storeName: this.props.storeName,
			storeAddress: this.props.storeAddress,
			storeId: this.props.storeId
		});
	}

	Validate() {
		let isError = false;

		if (this.state.storeName == '') {
			isError = true;
			this.setState({
				...this.state,
				storeNameError: "Store Name Required"
			});
		} else {
			this.setState({
				storeNameError: ""
			});
		}
		if (this.state.storeAddress == '') {
			isError = true;
			this.setState({
				...this.state,
				storeAddressError: "Store Address Required"
			});
		}
		else {
			this.setState({
				storeAddressError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				storeName: '',
				storeAddress: ''
			});

			const newData = {
				Id: this.state.storeId,
				storeName: this.state.storeName,
				storeAddress: this.state.storeAddress
			}

			axios({
				method: 'edit',
				url: '/Stores/EditStore',
				data: newData
			})
				.then(response => {
					//console.log(response.data);
					this.setState({ data: response.data, showModal: false });
				})
				.catch(error => {
					console.log(error)
				})
		}
	}


	handleChange(event) {
		this.Validate();
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="editstoreModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="red"> <Icon name='edit' />Edit</Button>} closeIcon>
					<Header content="Update Store Record" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="storeId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblName">Store Name</label>
									<div ><font color="red">{this.state.storeNameError}</font></div>
									<input type="text" id="storeName" defaultValue={this.state.storeName}
										name="storeName" onChange={this.handleChange}
										placeholder="Store Name" className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblAddress">Store Address</label>
									<div ><font color="red">{this.state.storeAddressError}</font></div>
									<input type="text" id="storeAddress"
										defaultValue={this.state.storeAddress}
										name="storeAddress" onChange={this.handleChange}
										placeholder="Store Address" className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default EditStoreModal;