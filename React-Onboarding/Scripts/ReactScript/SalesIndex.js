﻿import React from 'react';
import ReactDOM from 'react-dom';
import Customer from './Customer';
import 'semantic-ui-css/semantic.min.css';
import SalesTable from './SalesTable';
import SalesModal from './SalesModal';
//import { CustomerTableRow } from './CustomerTableRow';
//import TodoApp from './ShoppingList';


ReactDOM.render(
	<div>
		<SalesModal/>,
		<SalesTable />
	</div>,
	document.getElementById('salesIndex')
);

