﻿import React, { Component } from 'react';
import { edit,trash,Button, Buttondelete, Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import DeleteCustomerModal from './DeleteCustomerModal';
import EditCustomerModal from './EditCustomerModal';

		
class CustomerTableRow extends React.Component {
	
	render() {
		
		return (
			
			<Table.Row>
				<Table.Cell>{this.props.name}</Table.Cell>
				<Table.Cell>{this.props.address}</Table.Cell>
				<Table.Cell>{<EditCustomerModal customerId={this.props.customerId} customerName={this.props.name} customerAddress={this.props.address} />}</Table.Cell>
				<Table.Cell>{< DeleteCustomerModal customerId={this.props.customerId}/>}</Table.Cell>
			</Table.Row>
		);

	}
}
export default CustomerTableRow;

