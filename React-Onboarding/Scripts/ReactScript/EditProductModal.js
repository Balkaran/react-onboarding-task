﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';

class EditProductModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			productName: "", productPrice: "",
			showModal: false, productNameError: '', productPriceError: ''
		};
		this.handleOpen = this.handleOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.btnSave = this.btnSave.bind(this);
	}

	handleOpen() {
		this.setState({
			showModal: true, productName: this.props.productName,
			productPrice: this.props.productPrice,
			productId: this.props.productId
		});
	}

	Validate() {
		let isError = false;

		if (this.state.productName == '') {
			isError = true;
			this.setState({
				...this.state,
				productNameError: "Product Name Required"
			});
		} else {
			this.setState({
				productNameError: ""
			});
		}
		if (this.state.productPrice == '') {
			isError = true;
			this.setState({
				...this.state,
				productPriceError: "Product Price Required"
			});
		}
		else {
			this.setState({
				productPriceError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				productName: '',
				productPrice: ''
			});

			const newData = {
				Id: this.state.productId,
				productName: this.state.productName,
				productPrice: this.state.productPrice
			}

			axios({
				method: 'edit',
				url: '/Products/EditProduct',
				data: newData
			})
				.then(response => {
					//console.log(response.data);
					this.setState({ data: response.data, showModal: false });
				})
				.catch(error => {
					console.log(error)
				})
		}
	}
	
	handleChange(event) {
		this.Validate();
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="editProductModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="red"> <Icon name='edit' />Edit</Button>} closeIcon>
					<Header content="Update Customer Record" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="productId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblName">Product Name</label>
									<div ><font color="red">{this.state.productNameError}</font></div>
									<input type="text" id="productName" defaultValue={this.state.productName}
										name="productName" onChange={this.handleChange}
										placeholder="Product Name" className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblPrice">Product Price</label>
									<div ><font color="red">{this.state.productPriceError}</font></div>
									<input type="text" id="productPrice"
										defaultValue={this.state.productPrice}
										name="productPrice" onChange={this.handleChange}
										placeholder="Product Price" className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}
}
export default EditProductModal;