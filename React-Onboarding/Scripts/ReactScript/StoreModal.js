﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';


class StoreModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			StoreName: '', StoreAddress: '',
			storeNameError: '', storeAddressError: '', showModal: false
		};
		this.closeModal = this.closeModal.bind(this);
		this.btnSave = this.btnSave.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleOpen = this.handleOpen.bind(this);
	}

	handleChange(event) {
		this.Validate();
		this.setState({
			[event.target.name]: event.target.value
		});
	}
	handleOpen() {
		this.setState({ showModal: true });
	}

	Validate() {
		let isError = false;

		if (this.state.StoreName == '') {
			isError = true;
			this.setState({
				...this.state,
				storeNameError: "Store Name Required"
			});
		} else {
			this.setState({
				storeNameError: ""
			});
		}
		if (this.state.StoreAddress == '') {
			isError = true;
			this.setState({
				...this.state,
				storeAddressError: "Store Address Required"
			});
		}
		else {
			this.setState({
				storeAddressError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				StoreName: '',
				StoreAddress: ''
			});

			const newData = {
				StoreName: this.state.StoreName,
				StoreAddress: this.state.StoreAddress
			}

			axios({
				method: 'post',
				url: '/Stores/AddStores',
				data: newData
			})
				.then(response => {
					console.log(response.data);
					this.setState({ data: response.data, showModal: false });
					//window.location("href:");
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="storeModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="blue">Add New Store</Button>} closeIcon>
					<Header content="Add New Store" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="storeId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblName">Store Name</label>
									<div ><font color="red">{this.state.storeNameError}</font></div>
									<input type="text" id="storeName" name="StoreName"
										onChange={this.handleChange} placeholder="Store Name"
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblAddress">Store Address</label>
									<div ><font color="red">{this.state.storeAddressError}</font></div>
									<input type="text" id="storeAddress" name="StoreAddress"
										onChange={this.handleChange} placeholder="Store Address"
										className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default StoreModal;