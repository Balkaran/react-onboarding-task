﻿import React from 'react';
import ReactDOM from 'react-dom';
import Customer from './Customer';
import 'semantic-ui-css/semantic.min.css';
import ProductTable from './ProductTable';
import ProductModal from './ProductModal';
//import { CustomerTableRow } from './CustomerTableRow';
//import TodoApp from './ShoppingList';


ReactDOM.render(
	<div>
		<ProductModal />
		<ProductTable />
	</div>,
	document.getElementById('productIndex')
);

