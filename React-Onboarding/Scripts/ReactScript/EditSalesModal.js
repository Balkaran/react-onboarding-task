﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal, Dropdown } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
//import DatePicker from 'react-datepicker';
//import DayPickerInput from 'react-day-picker/DayPickerInput';

class EditSalesModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			Customers: [], Products: [], Stores: [],
			CustomerName: "", ProductName: "", StoreName: "",
			DateSold: "", SalesId: "", showModal: false,
			customerNameError: '', productNameError: '',
			storteNameError: '', dateSoldError: '', CustomerId: '',
			ProductId: '', StoreId: ''/*, startDate: new Date()*/
		};
		this.handleOpen = this.handleOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handletextChange = this.handletextChange.bind(this);
		this.btnSave = this.btnSave.bind(this);
		//this.handleDayChange = this.handleDayChange.bind(this);
	}

	handleOpen() {

		axios.get('/Customers/GetCustomerData')
			.then(res => {
				const Customers = res.data;
				//console.log(Customers);
				this.setState({ Customers });
			})

		axios.get('/Products/GetProductData')
			.then(res => {
				const Products = res.data;
				this.setState({ Products });
			})

		axios.get('/Stores/GetStoreData')
			.then(res => {
				const Stores = res.data;
				this.setState({ Stores });
			})
		this.setState({
			showModal: true,
			CustomerName: this.props.customername,
			ProductName: this.props.productname,
			StoreName: this.props.storename,
			DateSold: this.props.datesold,
			SalesId: this.props.salesId,
			CustomerId: this.props.customerId,
			ProductId: this.props.productId,
			StoreId: this.props.storeId
		});
		//console.log(this.props.customerId);
	}

	Validate() {
		let isError = false;
		if (this.state.CustomerName == '') {
			isError = true;
			this.setState({
				...this.state,
				customerNameError: "Customer Name Required"
			});
		} else {
			this.setState({
				customerNameError: ""
			});
		}
		if (this.state.ProductName == '') {
			isError = true;
			this.setState({
				...this.state,
				productNameError: "Product Name Required"
			});
		}
		else {
			this.setState({
				productNameError: ""
			});
		}
		if (this.state.StoreName == '') {
			isError = true;
			this.setState({
				...this.state,
				storteNameError: "Store Name Required"
			});
		}
		else {
			this.setState({
				storteNameError: ""
			});
		}
		if (this.state.DateSold == '') {
			isError = true;
			this.setState({
				...this.state,
				dateSoldError: "DateSold Required"
			});
		}
		else {
			this.setState({
				dateSoldError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				CustomerName: '',
				ProductName: '',
				StoreName: '',
				DateSold: ''
			});

			const newData = {
				Id: this.state.SalesId,
				CustomerId: this.state.CustomerId,
				ProductId: this.state.ProductId,
				StoreId: this.state.StoreId,
				DateSold: this.state.DateSold
			}

			axios({
				method: 'edit',
				url: '/Sales/EditSales',
				data: newData
			})
				.then(response => {
					//console.log(response.data);
					this.setState({ data: response.data, showModal: false });
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	handletextChange(event) {
		this.Validate();
		this.setState({
			DateSold: event.target.value
		});
	}

	//handleDayChange(date) {
	//	this.setState({ startDate: date });
	//}

	handleChange(e, { name, value }) {
		this.Validate();
		this.setState({
			[name]: value
		});
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="editsaleModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="red"> <Icon name='edit' />Edit</Button>} closeIcon>
					<Header content="Update Store Record" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="salesId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="customerLblName">Customer Name</label>
									<div ><font color="red">{this.state.customerNameError}</font></div>
									<Dropdown name="CustomerId"
										onChange={this.handleChange}
										fluid search selection options={this.state.Customers.map(customer => ({
											//Key: customer.Id,
											value: customer.Id,
											text: customer.CustomerName,
										}))}
										//placeholder={this.props.customername}
										defaultValue={this.state.CustomerId}
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblName">Product Name</label>
									<div ><font color="red">{this.state.productNameError}</font></div>
									<Dropdown name="ProductId"
										onChange={this.handleChange}
										fluid search selection options={this.state.Products.map(product => ({
											value: product.Id,
											text: product.ProductName,
										}))}
										//placeholder={this.props.productname}
										defaultValue={this.state.ProductId}
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblName">Store Name</label>
									<div ><font color="red">{this.state.storteNameError}</font></div>
									<Dropdown name="StoreId"
										onChange={this.handleChange}
										fluid search selection options={this.state.Stores.map(store => ({
											value: store.Id,
											text: store.StoreName,
										}))}
										//placeholder={this.props.storename}
										defaultValue={this.state.StoreId}
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="salesLblDate">Sold Date</label>
									<div ><font color="red">{this.state.dateSoldError}</font></div>
									<input type="text" name="DateSold"
										onChange={this.handletextChange}
										defaultValue={this.state.DateSold}
										className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default EditSalesModal;



//<DatePicker defaultValue={this.state.DateSold}
//	name="DateSold"
//	selected={this.state.startDate}
//	onChange={this.handleDayChange} />