﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import { fail } from 'assert';



class CustomerModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			CustomerName: '', CustomerAddress: '',
			showModal: false, customerNameError: '',
			customerAddressError: ''
		};
		this.closeModal = this.closeModal.bind(this);
		this.btnSave = this.btnSave.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleOpen = this.handleOpen.bind(this);
	}
	
	handleChange(event) {
		this.Validate();
		this.setState({
			//CustomerName: event.target.value,
			[event.target.name]: event.target.value
		});
	}
	handleOpen() {
		this.setState({ showModal: true });
	}

	Validate() {
		let isError = false;
		
		if (this.state.CustomerName == '') {
			isError = true;
			this.setState({
				...this.state,
				customerNameError: "Customer Name Required"
			});
		} else {
			this.setState({
				customerNameError: ""
			});
		}
		if (this.state.CustomerAddress == '') {
			isError = true;
			this.setState({
				...this.state,
				customerAddressError: "Customer Address Required"
			});
		}
		else {
			this.setState({
				customerAddressError: ""
			});
		}
		return isError;
	}

	btnSave() {		
		const err = this.Validate();
		if (!err) {
			this.setState({
				CustomerName: '',
				CustomerAddress: ''
			});

			const newData = {
				CustomerName: this.state.CustomerName,
				CustomerAddress: this.state.CustomerAddress
			}

			axios({
				method: 'post',
				url: '/Customers/AddCustomers',
				data: newData
			})
				.then(response => {
					//console.log(response.data);
					this.setState({
						data: response.data,
						showModal: false
					});
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="custModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="blue">Add New Customer</Button>} closeIcon>
					<Header content="Add New Customer" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="customerId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="custLblName">Customer Name</label>
									<div ><font color="red">{this.state.customerNameError}</font></div>
									<input type="text" id="custName"
										name="CustomerName"
										onChange={this.handleChange}
										placeholder="Customer Name"
										//errortext={this.state.customerNameError}
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="custLblAddress">Customer Address</label>
									<div ><font color="red">{this.state.customerAddressError}</font></div>
									<input type="text" id="custAddress"
										name="CustomerAddress"
										onChange={this.handleChange}
										placeholder="Customer Address"
										//errortext={this.state.customerAddressError}
										className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default CustomerModal;






























//const CustomerModal = () => (
	//<div>
	//	<Modal size="tiny" trigger={<Button color="blue">Add New Customer</Button>}>
	//	<Header icon="archive" content="Add New Customer" />
	//		<Modal.Content>
	//			<form id="myForm">
	//				<fieldset id="submittForm">
	//					<div className="form-group">
	//						<input type="hidden" id="customerId" className="form-control"/>
	//					</div>
	//					<div className="form-group">
	//						<label id="custLblName">Customer Name</label>
	//						<input type="text" id="custName" placeholder="Customer Name" className="form-control" />
	//					</div>
	//					<div className="form-group">
	//						<label id="custLblAddress">Customer Address</label>
	//						<input type="text" id="custAddress" placeholder="Customer Address" className="form-control"/>
	//					</div>
	//				</fieldset>
	//			</form>
	//		</Modal.Content>
	//		<Modal.Actions>
	//			<Button color="red">
	//				<Icon name="remove" />Cancel
	//			</Button>
	//			<Button color="green">
	//				<Icon name="checkmark" />Save
	//			</Button>
	//		</Modal.Actions>
	//	</Modal>
	//</div>
//)