﻿import React, { Component } from 'react';
import { edit, trash, Button, Buttondelete, Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import DeleteStoreModal from './DeleteStoreModal';
import EditStoreModal from './EditStoreModal';


class StoreTableRow extends React.Component {

	render() {
		return (
			<Table.Row>
				<Table.Cell>{this.props.name}</Table.Cell>
				<Table.Cell>{this.props.address}</Table.Cell>
				<Table.Cell>{<EditStoreModal storeId={this.props.storeId} storeName={this.props.name} storeAddress={this.props.address} />}</Table.Cell>
				<Table.Cell>{< DeleteStoreModal storeId={this.props.storeId} />}</Table.Cell>
			</Table.Row>
		);
	}
}
export default StoreTableRow;

