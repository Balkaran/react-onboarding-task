﻿import React, { Component } from 'react';
import { Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import Axios from 'axios';
import SalesTableRow from './SalesTableRow';
import StoreModal from './StoreModal';
import moment from 'moment';


class SalesTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = { data: []	};
	}

	componentDidMount() {
		axios({
			method: 'get',
			url: '/Sales/GetSalesData'
		})
			.then(response => {				
			const data = response.data;
			//console.log(data);
			this.setState({ data });
		})
		.catch(error => {
			console.log(error)
		})
	}

	componentDidUpdate() {
		axios({
			method: 'get',
			url: '/Sales/GetSalesData'
		})
			.then(response => {
				const data = response.data;
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}

	render() {
		var rows = [];
		this.state.data.forEach(function (sales) {
			//console.log(JSON.stringify(sales));
			rows.push(<SalesTableRow key={sales.Id}
				salesId={sales.Id}
				customerId={sales.Customer.Id}
				productId={sales.Product.Id}
				storeId={sales.Store.Id}
				customername={sales.Customer.CustomerName}
				productname={sales.Product.ProductName}
				storename={sales.Store.StoreName}
				datesold={moment(sales.DateSold).format()} />);
		});
		
		return (
			<Table celled>
				<Table.Header >
					<Table.Row>
						<Table.HeaderCell>Customer Name</Table.HeaderCell>
						<Table.HeaderCell>Product Name</Table.HeaderCell>
						<Table.HeaderCell>Store Name</Table.HeaderCell>
						<Table.HeaderCell>Date Sold</Table.HeaderCell>
						<Table.HeaderCell>Action(Edit)</Table.HeaderCell>
						<Table.HeaderCell>Action(Delete)</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				<Table.Body>
					{rows}
				</Table.Body>

				<Table.Footer>
				</Table.Footer>
			</Table>
		);
	}
}
export default SalesTable;















//Axios.all([
		//	Axios.get('/Customers/GetCustomerData'),
		//	Axios.get('/Products/GetProductData'),
		//	Axios.get('/Stores/GetStoreData')
		//	//Axios.get('/Sales/GetSalesData')
		//]).then(Axios.spread((customer, product, store) => {
		//	const options = {
		//		customer: customer.data,
		//		product: product.data,
		//		store: store.data
		//		//sale: sale.data
		//	};
		//	console.log(options);
		//	this.setState({ ddlOptions: options });
		//}))
		//	.catch(error => { console.log(error) })