﻿import React, { Component } from 'react';
import { Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import StoreTableRow from './StoreTableRow';
import StoreModal from './StoreModal';


class StoreTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = { data: [] };
	}

	componentDidMount() {
		axios({
			method: 'get',
			url: '/Stores/GetStoreData'
		})
			.then(response => {
				const data = response.data;
				//console.log(data);
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}

	componentDidUpdate() {
		axios({
			method: 'get',
			url: '/Stores/GetStoreData'
		})
			.then(response => {
				const data = response.data;
				//console.log(data);
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}

	render() {

		var rows = [];
		this.state.data.forEach(function (store) {
			rows.push(<StoreTableRow key={store.Id}
				storeId={store.Id}
				name={store.StoreName}
				address={store.StoreAddress} />);
		});
		return (
			<Table celled>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Store Name</Table.HeaderCell>
						<Table.HeaderCell>Store Address</Table.HeaderCell>
						<Table.HeaderCell>Action(Edit)</Table.HeaderCell>
						<Table.HeaderCell>Action(Delete)</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				<Table.Body>
					{rows}
				</Table.Body>

				<Table.Footer>
				</Table.Footer>
			</Table>
		);
	}

}
export default StoreTable;



