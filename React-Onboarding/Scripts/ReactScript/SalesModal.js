﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal, Dropdown } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';
import CustomerTable from './CustomerTable';
import { error } from 'util';
//import { CustomerTableRow } from './CustomerTableRow';


class SalesModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			Customers: [], Products: [], Stores: [],
			DateSold: '', showModal: false, CustomerName: '',
			ProductName: '', StoreName: '', customerNameError: '',
			productNameError: '', storteNameError: '', dateSoldError: ''
		};
		this.closeModal = this.closeModal.bind(this);
		this.btnSave = this.btnSave.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handletextChange = this.handletextChange.bind(this);
		this.handleOpen = this.handleOpen.bind(this);
	}

	handleChange(e, { name, value }) {
				this.setState({
				[name]: value
			});
	}

	handletextChange(e) {
		this.Validate();
		this.setState({
			DateSold: e.target.value
		});
	}

	handleOpen() {
		axios.get('/Customers/GetCustomerData')
			.then(res => {
				const Customers = res.data;
				//console.log(Customers);
				this.setState({ Customers });
			})

		axios.get('/Products/GetProductData')
			.then(res => {
				const Products = res.data;
				this.setState({ Products });
			})

		axios.get('/Stores/GetStoreData')
			.then(res => {
				const Stores = res.data;
				this.setState({ Stores });
			})
		this.setState({ showModal: true });
	}

	Validate() {
		let isError = false;

		if (this.state.CustomerName == '') {
			isError = true;
			this.setState({
				...this.state,
				customerNameError: "Customer Name Required"
			});
		} else {
			this.setState({
				customerNameError: ""
			});
		}
		if (this.state.ProductName == '') {
			isError = true;
			this.setState({
				...this.state,
				productNameError: "Product Name Required"
			});
		}
		else {
			this.setState({
				productNameError: ""
			});
		}
		if (this.state.StoreName == '') {
			isError = true;
			this.setState({
				...this.state,
				storteNameError: "Store Name Required"
			});
		}
		else {
			this.setState({
				storteNameError: ""
			});
		}
		if (this.state.DateSold == '') {
			isError = true;
			this.setState({
				...this.state,
				dateSoldError: "DateSold Required"
			});
		}
		else {
			this.setState({
				dateSoldError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				CustomerName: '',
				ProductName: '',
				StoreName: '',
				DateSold:''
			});

			const newData = {
				CustomerId: this.state.CustomerName,
				ProductId: this.state.ProductName,
				StoreId: this.state.StoreName,
				DateSold: this.state.DateSold
			}
			//console.log(newData);
			axios({
				method: 'post',
				url: '/Sales/AddSales',
				data: newData
			})
				.then(response => {
					//console.log(response.data);
					this.setState({ data: response.data, showModal: false });
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="salesModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="blue">Add New Sales Record</Button>} closeIcon>
					<Header content="Add New Sales Record" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="salesId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="customerLblName">Customer Name</label>
									<div ><font color="red">{this.state.customerNameError}</font></div>
									<Dropdown name="CustomerName" onChange={this.handleChange}
										placeholder="Select Customer" fluid search selection options={this.state.Customers.map(customer => ({
										//Key: customer.Id,
										value: customer.Id,
										text: customer.CustomerName,
									}))} className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblName">Product Name</label>
									<div ><font color="red">{this.state.productNameError}</font></div>
									<Dropdown name="ProductName" onChange={this.handleChange}
										placeholder="Select Product" fluid search selection options={this.state.Products.map(product => ({
										value: product.Id,
										text: product.ProductName,
									}))} className="form-control" />
								</div>
								<div className="form-group">
									<label id="storeLblName">Store Name</label>
									<div ><font color="red">{this.state.storteNameError}</font></div>
									<Dropdown name="StoreName" onChange={this.handleChange}
										placeholder="Select Store" fluid search selection options={this.state.Stores.map(store => ({
										value: store.Id,
										text: store.StoreName,
									}))} className="form-control" />
								</div>
								<div className="form-group">
									<label id="salesLblDate">Sold Date</label>
									<div ><font color="red">{this.state.dateSoldError}</font></div>
									<input type="text" name="DateSold" onChange={this.handletextChange}
										placeholder="DD/MM/YYYY" className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default SalesModal;