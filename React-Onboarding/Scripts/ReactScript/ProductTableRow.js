﻿import React, { Component } from 'react';
import { edit, trash, Button, Buttondelete, Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import DeleteProductModal from './DeleteProductModal';
import EditProductModal from './EditProductModal';


class ProductTableRow extends React.Component {

	render() {
		return (
			<Table.Row>
				<Table.Cell>{this.props.name}</Table.Cell>
				<Table.Cell>{this.props.price}</Table.Cell>
				<Table.Cell>{<EditProductModal productId={this.props.productId} productName={this.props.name} productPrice={this.props.price} />}</Table.Cell>
				<Table.Cell>{< DeleteProductModal productId={this.props.productId} />}</Table.Cell>
			</Table.Row>
		);

	}
}
export default ProductTableRow;

