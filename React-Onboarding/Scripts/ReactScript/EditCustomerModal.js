﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';

class EditCustomerModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			customerName: "", customerAddress: "",
			showModal: false, customerNameError: '',
			customerAddressError: ''
		};
		this.handleOpen = this.handleOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.btnSave = this.btnSave.bind(this);
	}

	handleOpen() {
		this.setState({
			showModal: true, customerName: this.props.customerName,
			customerAddress: this.props.customerAddress,
			customerId: this.props.customerId
		});
	}

	Validate() {
		let isError = false;

		if (this.state.customerName == '') {
			isError = true;
			this.setState({
				...this.state,
				customerNameError: "Customer Name Required"
			});
		} else {
			this.setState({
				customerNameError: ""
			});
		}
		if (this.state.customerAddress == '') {
			isError = true;
			this.setState({
				...this.state,
				customerAddressError: "Customer Address Required"
			});
		}
		else {
			this.setState({
				customerAddressError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				customerName: '',
				customerAddress: ''
			});

			const newData = {
				Id: this.state.customerId,
				customerName: this.state.customerName,
				customerAddress: this.state.customerAddress
			}
			
			axios({
				method: 'edit',
				url: '/Customers/EditCustomer',
				data: newData
			})
				.then(response => {
					console.log(response.data);
					this.setState({ data: response.data, showModal: false });
				})
				.catch(error => {
					console.log(error)
				})
		}
	}
	
	handleChange(event) {
		this.Validate();
			this.setState({
			[event.target.name]: event.target.value
		});
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		
		return (
			<div>
				<Modal id="editCustomerModal" open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="red"> <Icon name='edit' />Edit</Button>} closeIcon>
					<Header content="Update Customer Record" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="customerId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="custLblName">Customer Name</label>
									<div ><font color="red">{this.state.customerNameError}</font></div>
									<input type="text" id="custName"
										defaultValue={this.state.customerName}
										name="customerName" onChange={this.handleChange}
										placeholder="Customer Name" className="form-control" />
								</div>
								<div className="form-group">
									<label id="custLblAddress">Customer Address</label>
									<div ><font color="red">{this.state.customerAddressError}</font></div>
									<input type="text" id="custAddress"
										defaultValue={this.state.customerAddress}
										name="customerAddress" onChange={this.handleChange}
										placeholder="Customer Address" className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>

					</Modal.Actions>

				</Modal>
			</div>
		);
	}

}
export default EditCustomerModal;