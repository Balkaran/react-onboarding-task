﻿import React, { Component } from 'react';
import { Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import ProductTableRow from './ProductTableRow';
import ProductModal from './ProductModal';


class ProductTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = { data: [] };
	}

	componentDidMount() {
		axios({
			method: 'get',
			url: '/Products/GetProductData'
		})
			.then(response => {
				const data = response.data;
				//console.log(data);
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}

	componentDidUpdate() {
		axios({
			method: 'get',
			url: '/Products/GetProductData'
		})
			.then(response => {
				const data = response.data;
				//console.log(data);
				this.setState({ data });
			})
			.catch(error => {
				console.log(error)
			})
	}

	render() {

		var rows = [];
		this.state.data.forEach(function (product) {
			rows.push(<ProductTableRow key={product.Id}
				productId={product.Id} name={product.ProductName}
				price={product.ProductPrice} />);
		});
		return (
			<Table celled>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Product Name</Table.HeaderCell>
						<Table.HeaderCell>Product Price</Table.HeaderCell>
						<Table.HeaderCell>Action(Edit)</Table.HeaderCell>
						<Table.HeaderCell>Action(Delete)</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				<Table.Body>
					{rows}
				</Table.Body>

				<Table.Footer>
				</Table.Footer>
			</Table>
		);
	}

}
export default ProductTable;



