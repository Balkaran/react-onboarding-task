﻿import React from "react"
import ReactDOM from "react-dom"
import { Button, Header, Icon, Modal, Label, Form, Table, Menu, Popup, Message, Dropdown } from "semantic-ui-react"
import 'semantic-ui-css/semantic.min.css'
import axios from 'axios';
import Nav from './Navbar'
class Sales extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datas: [], Products: [], Customers: [], Stores: [], title: 'Sale Details', m_title: '', open: false, customerdata: [], size: 'small', Id: '', customerName: '', productName: '', storeName: '', dateSold: '', customerNameError: '', productNameError: '', storeNameError: '',
		};
		this.show = this.show.bind(this);///binding only for functions
	}
	componentDidUpdate(prevProps, prevState) {
		axios.get('/Sales/GetAllSales')
			.then(res => {
				this.setState({ datas: JSON.parse(res.data) });
			});

	}//no need to use location.reload()
	componentDidMount() {
		axios.get('/Sales/GetAllSales')
			.then(res => {
				this.setState({ datas: JSON.parse(res.data) });
			});
	}
	show(event) {
		var Id = event.target.id;
		event.preventDefault();
		var fn;
		axios.get('/Products/GetAllProducts')
			.then(res => {
				const Products = res.data;
				this.setState({ Products });
			})
		axios.get('/Customers/GetAllCustomers')
			.then(res => {
				const Customers = res.data;
				this.setState({ Customers });
			})
		axios.get('/Stores/GetAllStores')
			.then(res => {
				const Stores = res.data;
				this.setState({ Stores });
			})
		if (event.target.value === '') {
			fn = 'Create';
			this.setState({ Id: '', customerName: '', productName: '', storeName: '', dateSold: '' })
		}
		else {
			var form_ID = Id;
			fn = event.target.value;


			axios.get('Sales/GetSales' + Id)
				.then(res => {
					const Id = res.data.ID;
					const customerName = res.data.Customer.Name;
					const productName = res.data.Product.Name;
					const storeName = res.data.Store.Name;
					const dateSold = res.data.Datesold;
					this.setState({ Id, customerName, productName, storeName, dateSold });
				})
		}
		this.setState(state => ({
			size: 'small',
			open: true,
			m_title: fn,
			nameError: '',
			addressError: ''
		}));
	}
	submit(m_title, Id, customerName, productName, storeName, dateSold) {
		console.log(Id, customerName, productName, storeName, dateSold);
		if (customerName != '' && productName != '' && storeName != '' && dateSold != '' && m_title === 'Create') {
			axios({
				method: 'post',
				url: '/Sales/Create/',
				data: {
					customerName: Customer.Name,
					productName: Product.Name,
					storeName: Store.Name,
					dateSold: Datesold,
				}
			});
			this.setState({ open: false })
		}
		else if (customerName != '' && productName != '' && storeName != '' && dateSold != '' && m_title === 'Edit') {
			axios({
				method: 'post',
				url: '/Sales/Edit/',
				data: {
					ID: Id,
					customerName: Customer.Name,
					productName: Product.Name,
					storeName: Store.Name,
					dateSold: Datesold,
				}
			});
			this.setState({ open: false })
		}
		else if (customerName != '' && productName != '' && storeName != '' && dateSold != '' && m_title === 'Delete') {
			axios({
				method: 'post',
				url: '/Sales/Delete/',
				data: {
					ID: Id
				}
			});
			this.setState({ open: false })
		}
		else {
			alert("Please fill all the Sale Details.");
			//if (Name == '' && Address == '') {
			//    this.setState({ nameError: 'Please enter Name', addressError: 'Please enter Address' })
			//}
			//if (Address == '' && Name != '') {
			//    this.setState({ nameError: '', addressError: 'Please enter Address' })
			//}
			//if (Address != '' && Name == '') {
			//    this.setState({ nameError: 'Please enter Name', addressError: '' })
			//}
		}
	}
	close = () => this.setState({ open: false })
	render() {
		const { open, size, datas, Customers, Stores, Products } = this.state


		return (
			<div>

				<h2> {this.state.title}</h2>
				<br />
				<Button color='blue' id="btnCreate" onClick={this.show}>
					<i aria-hidden='true' className='plus icon' /> Add New Sale
                     </Button>
				<Modal size="small" open={open} onClose={this.close} closeIcon>
					<Modal.Header>{this.state.m_title}</Modal.Header>
					<Modal.Content id="Add_Edit">
						<Form key={this.state.Id}>
							<input id="m_title" value={this.state.m_title} hidden />
							<input type='text' id="Id" defaultValue={this.state.Id} hidden />
							<Form.Field>
								<label>Customer Name</label>
								<div ><font color="red">{this.state.customerNameError}</font></div>
								<Dropdown id='customerName' placeholder='Select Country' fluid search selection options={Customers.map(customer => ({
									key: customer.ID,
									value: customer.Name,
									text: customer.Name,
								}))} />

							</Form.Field>
							<Form.Field>
								<label>Product Name</label>
								<div ><font color="red">{this.state.productNameError}</font></div>
								<Dropdown id='productName' placeholder='Select Product' fluid search selection options={Products.map(product => ({
									key: product.ID,
									value: product.Name,
									text: product.Name,
								}))} />
							</Form.Field>
							<Form.Field>
								<label>Store Name</label>
								<div ><font color="red">{this.state.storeNameError}</font></div>
								<Dropdown id='storeName' placeholder='Select Store' fluid search selection options={Stores.map(store => ({
									key: store.ID,
									value: store.Name,
									text: store.Name,
								}))} />
							</Form.Field>
							<Form.Field>
								<label>Date Sold</label>
								<div ><font color="red">{this.state.dateSoldError}</font></div>
								<Dropdown id='dateSold' placeholder='Date Sold' fluid search selection options={this.state.dateSold} />
							</Form.Field>
						</Form>
					</Modal.Content>
					<Modal.Actions>
						<Button color='blue' type='submit' id="btnSubmit" onClick={() => {
							this.submit(m_title.value, Id.value, customerName.value, productName.value, storeName.value, dateSold.value)
						}}>Submit</Button>
						<Button color='red' onClick={this.close}>Close</Button>
					</Modal.Actions>
				</Modal>
				<Table celled selectable fixed >
					<Table.Header >
						<Table.Row active>
							<Table.HeaderCell>Customer Name</Table.HeaderCell>
							<Table.HeaderCell>Product Name</Table.HeaderCell>
							<Table.HeaderCell>Store Name</Table.HeaderCell>
							<Table.HeaderCell>Date Sold</Table.HeaderCell>
							<Table.HeaderCell>Action(Edit)</Table.HeaderCell>
							<Table.HeaderCell>Action(Delete)</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body >

						{datas.map((datas, index) => (
							<Table.Row key={index}>
								<Table.Cell>{datas.Customer.Name}</Table.Cell>
								<Table.Cell>{datas.Product.Name}</Table.Cell>
								<Table.Cell>{datas.Store.Name}</Table.Cell>
								<Table.Cell>{datas.Datesold}</Table.Cell>
								<Table.Cell>
									<Button color='yellow' id="btnEdit" onClick={this.show} id={datas.ID} value='Edit'>
										<i aria-hidden='true' className='pencil icon' />Edit
                                 </Button>
								</Table.Cell>
								<Table.Cell>
									<Button color='red' id="btnDelete" onClick={this.show} id={datas.ID} value='Delete' >
										<i aria-hidden='true' className='trash icon' /> Delete
                                 </Button>
								</Table.Cell>
							</Table.Row>
						))}



					</Table.Body>
				</Table>
			</div>
		);
	}
}
export default Sales;