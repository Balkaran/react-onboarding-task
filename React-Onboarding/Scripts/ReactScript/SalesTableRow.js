﻿import React, { Component } from 'react';
import { edit, trash, Button, Buttondelete, Icon, Label, Menu, Table } from 'semantic-ui-react';
import axios from 'axios';
import DeleteSalesModal from './DeleteSalesModal';
import EditSalesModal from './EditSalesModal';
import moment from 'moment';


class SalesTableRow extends React.Component {

	render() {
		
		return (
			
			<Table.Row>
				<Table.Cell>{this.props.customername}</Table.Cell>
				<Table.Cell>{this.props.productname}</Table.Cell>
				<Table.Cell>{this.props.storename}</Table.Cell>
				<Table.Cell>{this.props.datesold}</Table.Cell>
				<Table.Cell>{<EditSalesModal salesId={this.props.salesId} customerId={this.props.customerId} productId={this.props.productId} storeId={this.props.storeId} customername={this.props.customername} productname={this.props.productname} storename={this.props.storename} datesold={this.props.datesold} />}</Table.Cell>
				<Table.Cell>{< DeleteSalesModal salesId={this.props.salesId} />}</Table.Cell>
			</Table.Row>
		);

	}
}
export default SalesTableRow;

