﻿import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import StoreTable from './StoreTable';
import StoreModal from './StoreModal';



ReactDOM.render(
	<div>
		<StoreModal />
		<StoreTable />
	</div>,
	document.getElementById('storeIndex')
);

