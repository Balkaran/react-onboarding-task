﻿import React, { Component } from 'react';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';
import axios from 'axios';
import 'semantic-ui-css/semantic.min.css';


class ProductModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			ProductName: '', ProductPrice: '',
			productNameError: '', productPriceError: '',
			showModal: false
		};
		this.closeModal = this.closeModal.bind(this);
		this.btnSave = this.btnSave.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleOpen = this.handleOpen.bind(this);
	}

	handleChange(event) {
		this.Validate();
		this.setState({
			[event.target.name]: event.target.value
		});
	}
	handleOpen() {
		this.setState({ showModal: true });
	}
	
	Validate() {
		let isError = false;

		if (this.state.ProductName == '') {
			isError = true;
			this.setState({
				...this.state,
				productNameError: "Product Name Required"
			});
		} else {
			this.setState({
				productNameError: ""
			});
		}
		if (this.state.ProductPrice == '') {
			isError = true;
			this.setState({
				...this.state,
				productPriceError: "Product Price Required"
			});
		}
		else {
			this.setState({
				productPriceError: ""
			});
		}
		return isError;
	}

	btnSave() {
		const err = this.Validate();
		if (!err) {
			this.setState({
				ProductName: '',
				ProductPrice: ''
			});

			const newData = {
				ProductName: this.state.ProductName,
				ProductPrice: this.state.ProductPrice
			}

			axios({
				method: 'post',
				url: '/Products/AddProducts',
				data: newData
			})
				.then(response => {
					console.log(response.data);
					this.setState({ data: response.data, showModal: false });
					//window.location("href:");
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		return (
			<div>
				<Modal id="productModal"  open={this.state.showModal} size="tiny" trigger={<Button onClick={this.handleOpen} color="blue">Add New Product</Button>} closeIcon>
					<Header content="Add New Customer" />
					<Modal.Content>
						<form id="myForm">
							<fieldset id="submittForm">
								<div className="form-group">
									<input type="hidden" id="productId" className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblName">Product Name</label>
									<div ><font color="red">{this.state.productNameError}</font></div>
									<input type="text" id="productName" name="ProductName"
										onChange={this.handleChange} placeholder="Product Name"
										className="form-control" />
								</div>
								<div className="form-group">
									<label id="productLblPrice">Product Price</label>
									<div ><font color="red">{this.state.productPriceError}</font></div>
									<input type="text" id="productPrice" name="ProductPrice"
										onChange={this.handleChange} placeholder="Product Price"
										className="form-control" />
								</div>
							</fieldset>
						</form>
					</Modal.Content>
					<Modal.Actions>
						<Button onClick={this.closeModal} color="red">
							<Icon name="remove" />Cancel
						</Button>
						<Button onClick={this.btnSave} color="green">
							<Icon name="checkmark" />Save
						</Button>
						
					</Modal.Actions>

				</Modal>
			</div>
			);
	}

}
export default ProductModal;