﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace React_Onboarding.Models
{
	public class Product
	{
		public int Id { get; set; }

		public string ProductName { get; set; }

		[DataType(DataType.Currency)]
		public decimal ProductPrice { get; set; }

		public ICollection<Sales> Sales { get; set; }
	}
}