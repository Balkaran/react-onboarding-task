﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace React_Onboarding.Models
{
	public class StoreDBContext:DbContext
	{
		public StoreDBContext(): base("name=React_Task3_DB")
		{

		}
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Store> Stores { get; set; }
		public DbSet<Sales> Sales { get; set; }
	}
}