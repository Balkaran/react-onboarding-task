﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace React_Onboarding.Models
{
	public class Store
	{
		public int Id { get; set; }

		public string StoreName { get; set; }

		public string StoreAddress { get; set; }

		public ICollection<Sales> Sales { get; set; }
	}
}