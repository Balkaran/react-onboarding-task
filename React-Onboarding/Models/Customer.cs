﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace React_Onboarding.Models
{
	public class Customer
	{
		public int Id { get; set; }

		public string CustomerName { get; set; }

		public string CustomerAddress { get; set; }

		public ICollection<Sales> Sales { get; set; }
	}
}