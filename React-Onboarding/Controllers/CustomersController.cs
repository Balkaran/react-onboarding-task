﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using React_Onboarding.Models;
using Newtonsoft.Json;

namespace React_Onboarding.Controllers
{
    public class CustomersController : Controller
    {
        private StoreDBContext db = new StoreDBContext();

		//GET: Customers
		public ActionResult Index()
		{
			return View();
		}

		public JsonResult GetCustomerData()
		{
			var data = db.Customers.ToList();
			return Json(data, JsonRequestBehavior.AllowGet);

		}

		//public JsonResult GetCustomerData()
		//{
		//	var data = db.Customers.ToList();
		//	return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

		//}

		// GET: Customers/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

		// POST: Customers/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Create([Bind(Include = "Id,CustomerName,CustomerAddress")] Customer customer)
		//{
		//    if (ModelState.IsValid)
		//    {
		//        db.Customers.Add(customer);
		//        db.SaveChanges();
		//        return RedirectToAction("Index");
		//    }

		//    return View(customer);
		//}

		public JsonResult AddCustomers(Customer customer)
		{
			if (ModelState.IsValid)
			{
				db.Customers.Add(customer);
				db.SaveChanges();
				return Json("Index");
			}

			return Json(customer,JsonRequestBehavior.AllowGet);
		}

		// GET: Customers/Edit/5
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,CustomerName,CustomerAddress")] Customer customer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(customer).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(customer);
        //}


		public JsonResult EditCustomer(Customer customer)
		{
			if (ModelState.IsValid)
			{
				db.Entry(customer).State = EntityState.Modified;
				db.SaveChanges();
				return Json("Index");
			}
			return Json(customer);
		}

		// GET: Customers/Delete/5
		//public ActionResult Delete(int? id)
		//{
		//    if (id == null)
		//    {
		//        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//    }
		//    Customer customer = db.Customers.Find(id);
		//    if (customer == null)
		//    {
		//        return HttpNotFound();
		//    }
		//    return View(customer);
		//}

		public JsonResult DeleteCustomer(int id)
		{
			Customer customer = db.Customers.Find(id);
			//if (customer == null)
			//{
			//	return NotFound();
			//}

			db.Customers.Remove(customer);
			db.SaveChanges();

			return Json(customer);
		}

		// POST: Customers/Delete/5
		[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
