﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using React_Onboarding.Models;

namespace React_Onboarding.Controllers
{
    public class ProductsController : Controller
    {
        private StoreDBContext db = new StoreDBContext();

        // GET: Products
        public ActionResult Index()
        {
			return View();
        }

		public JsonResult GetProductData()
		{
			var data = db.Products.ToList();
			return Json(data, JsonRequestBehavior.AllowGet);

		}

		// GET: Products/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,ProductName,ProductPrice")] Product product)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Products.Add(product);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(product);
        //}

		public JsonResult AddProducts(Product product)
		{
			if (ModelState.IsValid)
			{
				db.Products.Add(product);
				db.SaveChanges();
				return Json("Index");
			}

			return Json(product, JsonRequestBehavior.AllowGet);
		}

		// GET: Products/Edit/5
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

		// POST: Products/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit([Bind(Include = "Id,ProductName,ProductPrice")] Product product)
		//{
		//    if (ModelState.IsValid)
		//    {
		//        db.Entry(product).State = EntityState.Modified;
		//        db.SaveChanges();
		//        return RedirectToAction("Index");
		//    }
		//    return View(product);
		//}


		public JsonResult EditProduct(Product product)
		{
			if (ModelState.IsValid)
			{
				db.Entry(product).State = EntityState.Modified;
				db.SaveChanges();
				return Json("Index");
			}
			return Json(product);
		}

		//// GET: Products/Delete/5
		//public ActionResult Delete(int? id)
  //      {
  //          if (id == null)
  //          {
  //              return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
  //          }
  //          Product product = db.Products.Find(id);
  //          if (product == null)
  //          {
  //              return HttpNotFound();
  //          }
  //          return View(product);
  //      }

		public JsonResult DeleteProduct(int id)
		{
			Product product = db.Products.Find(id);
			//if (customer == null)
			//{
			//	return NotFound();
			//}

			db.Products.Remove(product);
			db.SaveChanges();

			return Json(product);
		}

		// POST: Products/Delete/5
		[HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
