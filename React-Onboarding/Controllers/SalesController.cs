﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using React_Onboarding.Models;

namespace React_Onboarding.Controllers
{
    public class SalesController : Controller
    {
        private StoreDBContext db = new StoreDBContext();

		// GET: Sales
		//public ActionResult Index()
		//{
		//    var sales = db.Sales.Include(s => s.Customer).Include(s => s.Product).Include(s => s.Store);
		//    return View(sales.ToList());
		//}

		public ActionResult Index()
		{
			//List<Sales> sales = db.Sales.Include(s => s.Customer).Include(s => s.Product).Include(s => s.Store).ToList();
			return View();
		}

		public JsonResult GetSalesData()
		{
			var sales = db.Sales.Select(s => new
			{
				s.Id,
				s.CustomerId,
				s.ProductId,
				s.StoreId,
				s.DateSold,
				s.Customer,
				s.Product,
				s.Store
			}).ToList();
			//List<Sales> sales = db.Sales.Include(s => s.Customer).Include(s => s.Product).Include(s => s.Store).ToList();
			return Json(sales,JsonRequestBehavior.AllowGet);
		}

		//public JsonResult GetSsalesData()
		//{
		//	var data = db.Sales.ToList();
		//	return Json(data, JsonRequestBehavior.AllowGet);

		//}

		// GET: Sales/Details/5
		public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales sales = db.Sales.Find(id);
            if (sales == null)
            {
                return HttpNotFound();
            }
            return View(sales);
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName");
            ViewBag.ProductId = new SelectList(db.Products, "Id", "ProductName");
            ViewBag.StoreId = new SelectList(db.Stores, "Id", "StoreName");
            return View();
        }

        // POST: Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,ProductId,CustomerId,StoreId,DateSold")] Sales sales)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Sales.Add(sales);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", sales.CustomerId);
        //    ViewBag.ProductId = new SelectList(db.Products, "Id", "ProductName", sales.ProductId);
        //    ViewBag.StoreId = new SelectList(db.Stores, "Id", "StoreName", sales.StoreId);
        //    return View(sales);
        //}

		//[HttpPost]
		//[ValidateAntiForgeryToken]
		public JsonResult AddSales(Sales sales)
		{
			if (ModelState.IsValid)
			{
				db.Sales.Add(sales);
				db.SaveChanges();
				return Json("Index");
			}
			return Json(sales);
		}

		// GET: Sales/Edit/5
		public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales sales = db.Sales.Find(id);
            if (sales == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", sales.CustomerId);
            ViewBag.ProductId = new SelectList(db.Products, "Id", "ProductName", sales.ProductId);
            ViewBag.StoreId = new SelectList(db.Stores, "Id", "StoreName", sales.StoreId);
            return View(sales);
        }

        // POST: Sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,ProductId,CustomerId,StoreId,DateSold")] Sales sales)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(sales).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", sales.CustomerId);
        //    ViewBag.ProductId = new SelectList(db.Products, "Id", "ProductName", sales.ProductId);
        //    ViewBag.StoreId = new SelectList(db.Stores, "Id", "StoreName", sales.StoreId);
        //    return View(sales);
        //}

		public JsonResult EditSales(Sales sales)
		{
			if (ModelState.IsValid)
			{
				db.Entry(sales).State = EntityState.Modified;
				db.SaveChanges();
				return Json("Index");
			}
			return Json(sales);
		}

		// GET: Sales/Delete/5
		public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales sales = db.Sales.Find(id);
            if (sales == null)
            {
                return HttpNotFound();
            }
            return View(sales);
        }

        // POST: Sales/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Sales sales = db.Sales.Find(id);
        //    db.Sales.Remove(sales);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

		public JsonResult DeleteSale(int id)
		{
			Sales sales = db.Sales.Find(id);
			db.Sales.Remove(sales);
			db.SaveChanges();
			return Json("Index");
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
